# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from itertools import chain
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls._transitions.add(
            ('processing', 'draft'),
            )
        cls._buttons['draft']['invisible'] &= (Eval('state') != 'processing')

    @classmethod
    def draft(cls, purchases):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        Invoice = pool.get('account.invoice')
        StockMove = pool.get('stock.move')
        Group = pool.get('res.group')
        ModelData = pool.get('ir.model.data')

        transaction = Transaction()

        user_id = transaction.user
        if user_id != 0:
            group_id = Group(ModelData.get_id(
                'purchase_processing2draft',
                'group_purchase_process2draft')).id
            groups = transaction.context['groups']

        to_write = []
        to_delete_invoices = []
        to_delete_moves = {}
        shipments = {
            'stock.shipment.in': set(),
            'stock.shipment.in.return': set()
        }
        shipment_state = cls._get_unprocess_shipment_state()
        for purchase in purchases:
            if purchase.state != 'processing':
                continue
            if user_id != 0 and group_id not in groups:
                raise UserError(gettext(
                    'purchase_processing2draft.'
                    'msg_purchase_user_group_purchase2draft'))

            to_delete_invoices.extend(
                    list(set(invoice for line in purchase.lines
                        for invoice in line.invoice_lines)))
            for move in purchase.moves or []:
                to_delete_moves.setdefault(move.origin, []).append(move)
            to_write.append(purchase)

        if to_write:
            cls.write(to_write, {'state': 'draft'})

        with Transaction().set_context(_check_access=False,
                check_origin=False, check_shipment=False):
            if to_delete_invoices:
                line_changes = {'origin': None}
                if hasattr(InvoiceLine, 'secondary_unit'):
                    line_changes.update({'secondary_unit': None})
                InvoiceLine.write(to_delete_invoices, line_changes)
                invoices = list(set([l.invoice for l in to_delete_invoices
                    if l.invoice]))
                InvoiceLine.delete(to_delete_invoices)
                if invoices:
                    lines = InvoiceLine.search([
                        ('invoice', 'in', list(map(int, invoices))),
                        ('type', '=', 'line')])
                    if not lines:
                        Invoice.delete(invoices)

            if to_delete_moves:
                for origin, moves in to_delete_moves.items():
                    for move in moves:
                        if not move.shipment:
                            continue
                        shipments[move.shipment.__name__].add(move.shipment.id)
                    StockMove.write(moves, {'origin': None})
                    StockMove.cancel(moves)
                if shipment_state == 'delete':
                    StockMove.delete(moves)

                for key, shipment_ids in shipments.items():
                    if not shipment_ids:
                        continue
                    other_moves = StockMove.search([
                        ('origin', 'like', 'purchase.line,%'),
                        ('shipment.id', 'in', shipment_ids, key)])
                    if other_moves:
                        raise UserError(gettext(
                            'purchase_processing2draft.'
                            'msg_purchase_delete_grouped_shipment',
                            shipment=other_moves[0].shipment.rec_name))
                    Shipment = Pool().get(key)
                    _shipments = Shipment.browse(shipment_ids)
                    Shipment.cancel(_shipments)
                    if shipment_state == 'delete':
                        Shipment.delete(_shipments)

        if shipment_state != 'delete' and to_delete_moves:
            # reassign origins
            changes = [(moves, {
                'origin': str(origin) if origin else None})
                for origin, moves in to_delete_moves.items()]
            StockMove.write(*list(chain(*changes)))

        # reset states
        cls._process_invoice_shipment_states(purchases)

        super(Purchase, cls).draft(purchases)

    @classmethod
    def _get_unprocess_shipment_state(cls):
        return 'delete'


class Purchase2(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    shipment_number_cache = fields.Char('Shipment number Cache', readonly=True)
    shipment_return_number_cache = fields.Char('Shipment number Cache',
        readonly=True)

    @classmethod
    def copy(cls, records, default=None):
        if default is None:
            default = {}
        else:
            default = default.copy()
        default.setdefault('shipment_number_cache', None)
        default.setdefault('shipment_return_number_cache', None)
        return super().copy(records, default=default)

    @classmethod
    def draft(cls, purchases):
        to_write = []
        for purchase in purchases:
            changes = {}
            if purchase.state != 'processing':
                continue
            if len(purchase.shipments) == 1:
                changes['shipment_number_cache'] = purchase.shipments[0].number
            if len(purchase.shipment_returns) == 1:
                changes['shipment_return_number_cache'] = \
                    purchase.shipment_returns[0].number
            to_write.extend([[purchase], changes])

        super().draft(purchases)

        if to_write:
            cls.write(*to_write)

    def _get_shipment_purchase(self, Shipment):
        shipment = super()._get_shipment_purchase(Shipment)
        types = {
            'stock.shipment.in': ('shipments', 'shipment_number_cache'),
            'stock.shipment.in.return': ('shipment_returns',
                'shipment_return_number_cache')
        }
        _type = types[Shipment.__name__]
        if not getattr(self, _type[0]) and getattr(self, _type[1], None):
            shipment.number = getattr(self, _type[1])
        return shipment
