# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class PurchaseProcessing2DraftTestCase(ModuleTestCase):
    """Test Purchase Processing2Draft module"""
    module = 'purchase_processing2draft'
    extras = ['purchase_shipment_method']


del ModuleTestCase
