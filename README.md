datalife_purchase_processing2draft
==================================

The purchase_processing2draft module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-purchase_processing2draft/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-purchase_processing2draft)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
